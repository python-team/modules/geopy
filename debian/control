Source: geopy
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Daniele Tricoli <eriol@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-docutils,
 python3-geographiclib,
 python3-looseversion,
 python3-pytest,
 python3-setuptools,
 python3-tz
Rules-Requires-Root: no
Standards-Version: 4.6.2.0
Homepage: https://github.com/geopy/geopy
Vcs-Git: https://salsa.debian.org/python-team/packages/geopy.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/geopy

Package: python3-geopy
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 python3-geographiclib
Recommends:
 python3-tz
Description: geocoding toolbox for Python3
 geopy makes it easy for developers to locate the coordinates of addresses,
 cities, countries, and landmarks across the globe using third-party geocoders
 and other sources of data, such as wikis.
 It also comes with the necessary means for parsing geographical coordinates
 and geodesic distance calculation (using great-circle distance or Vincenty
 distance). The distance module also contains useful routines for converting
 between length and angle units.
 .
 This package contains the Python 3 version of the library.
